const express = require("express")
const app = express()
const controllers = require("./controllers")
const swaggerUI = require("swagger-ui-express")
const cors = require('cors')

const { PORT = 8080 } = process.env

app.use(express.json())

// endpoint login
app.post("/api/v1/login", controllers.login)

// endpoint member
app.post("/api/v1/register", controllers.regist)

// endpoint admin
app.post("/api/v1/admins", controllers.adminAuthenticator, controllers.adminPost)
app.get("/api/v1/admins", controllers.adminAuthenticator, controllers.adminGetAll)
app.get("/api/v1/admins/:id", controllers.adminAuthenticator, controllers.adminGetByID)
app.put("/api/v1/admins/:id", controllers.adminAuthenticator, controllers.adminPut)
app.delete("/api/v1/admins/:id", controllers.adminAuthenticator, controllers.adminDelete)

// endpoint car
app.post("/api/v1/cars", controllers.carAuthenticator, controllers.carPost)
app.get("/api/v1/cars", controllers.carGetAll)
app.get("/api/v1/cars/:id", controllers.carGetByID)
app.put("/api/v1/cars/:id", controllers.carAuthenticator, controllers.carPut)
app.delete("/api/v1/cars/:id", controllers.carAuthenticator, controllers.carDelete)

// endpoint current user
app.get("/api/v1/profile", controllers.userCurrent)

// https://www.npmjs.com/package/swagger-ui-express
const options = {
    swaggerOptions: {
        url: "/api-docs"
    }
}
app.use("/docs", swaggerUI.serve, swaggerUI.setup(null, options))
app.get("/api-docs", (req, res) => {
    res.sendFile(__dirname + "/swagger.yaml")
})

app.listen(PORT, () => console.log(`listen on port ${PORT}`))

