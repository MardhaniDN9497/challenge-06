const jwt = require("jsonwebtoken");
const { Users } = require("../models");
const decryptPass = require("./decrypt_pass");

async function login(req, res) {
  try {
    let check = await Users.findOne({
      where: { username: req.body.username },
    });

    if (check) {
      let idGenerator = check.id;
      let validation = await Users.findOne({
        where: { id: idGenerator },
      });
      let checkPassword = await decryptPass(
        validation.password,
        req.body.password
      );
      if (req.body.username === validation.username && checkPassword) {
        let user = {
          id: validation.id,
          username: validation.username,
          role: validation.role,
        };
        let token = jwt.sign(user, "s3cr3t");
        res.status(200).json({
          token: token,
        });
        return;
      } else {
        res.send("invalid");
      }
    } else {
      res.send("invalid");
    }
  } catch (err) {
    res.send(err)
  }

}

module.exports = login
