module.exports = {
    // Login
    login: require("./login"),

    // Member
    regist: require("./register"),

    // Admin
    adminAuthenticator: require("./admin_authenticator"),
    adminPost: require("./admin_post"),
    adminGetAll: require("./admin_get_all"),
    adminGetByID: require("./admin_get_id"),
    adminPut: require("./admin_put"),
    adminDelete: require("./admin_delete"),

    // Car
    carAuthenticator: require("./car_authenticator"),
    carPost: require("./car_post"),
    carGetAll: require("./car_get_all"),
    carGetByID: require("./car_get_id"),
    carPut: require("./car_put"),
    carDelete: require("./car_delete"),

    // User
    userCurrent: require("./current_user"),

    // Encrypt for seeders data
    encryptPass: require("./encrypt_pass"),
};
