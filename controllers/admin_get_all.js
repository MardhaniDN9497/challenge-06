const { Users } = require("../models")

async function adminGetAll(req, res) {
    let listusers = await Users.findAll({
        where: { role: "admin", is_deleted: false },
    });

    let list = []
    for (let i in listusers) {
        list.push({
            id: listusers[i].id,
            username: listusers[i].username,
            password: listusers[i].password,
            address: listusers[i].address,
            phone_number: listusers[i].phone_number,
            fullname: listusers[i].fullname
        });
    }
    res.send(list);
}

module.exports = adminGetAll