const { Cars } = require("../models");
const { Users } = require("../models");

async function carGetAll(req, res) {
  try {
    let listCars = await Cars.findAll({ where: { is_deleted: false } });
    let listOfCars = [];
    for (let i in listCars) {
      let user1 = await Users.findOne({
        where: { id: listCars[i].created_by },
      });
      let user2 = await Users.findOne({
        where: { id: listCars[i].updated_by },
      });

      listOfCars.push({
        name: listCars[i].name,
        price: listCars[i].price,
        size: listCars[i].size_id,
        photo: listCars[i].photo,
        created_by: user1.username,
        updated_by: user2.username,
      });
    }
    res.send(listOfCars);
  } catch (err) {
    res.send(err)
  }

}

module.exports = carGetAll
