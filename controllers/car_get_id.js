const { Cars } = require("../models");
const { Users } = require("../models");

async function carGetByID(req, res) {
    try {
        let car = await Cars.findOne({
            where: { id: req.params.id, is_deleted: false },
        });
        if (car) {
            let user1 = await Users.findOne({
                where: { id: car.created_by },
            });
            let user2 = await Users.findOne({
                where: { id: car.updated_by },
            });
            res.json({
                id: car.id,
                name: car.name,
                price: car.price,
                size: car.size_id,
                photo: car.photo,
                created_by: user1.username,
                updated_by: user2.username,
            });
            return;
        }
        res.send("Data Mobil tidak ada");
        return;
    } catch (err) {
        res.send(err)
        return;
    }
}

module.exports = carGetByID
