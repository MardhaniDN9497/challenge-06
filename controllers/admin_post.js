const { Users } = require("../models")
const encryptFunction = require("./encrypt_pass");

async function adminPost(req, res) {
    try {
        let usernameInput = req.body.username;
        let passwordInput = await encryptFunction(req.body.password);
        let addressInput = req.body.address;
        let phone_numberInput = req.body.phone_number;
        let fullnameInput = req.body.fullname;
        let newuser = await Users.create({
            username: usernameInput,
            password: passwordInput,
            role: "admin",
            address: addressInput,
            phone_number: phone_numberInput,
            fullname: fullnameInput,
        });

        res.status(201).json({
            id: newuser.id,
            username: newuser.username,
            password: newuser.password,
            address: newuser.address,
            phone_number: newuser.phone_number,
            fullname: newuser.fullname,
            created_at: newuser.created_at,
            updated_at: newuser.updated_at,
        });
        return;
    } catch (err) {
        res.send(err)
    }
}


module.exports = adminPost