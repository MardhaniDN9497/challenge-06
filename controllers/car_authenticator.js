const jwt = require("jsonwebtoken")

function carAuthenticator(req, res, next) {
    try {
        let header = req.headers.authorization.split("Bearer ")[1]
        let user = jwt.verify(header, "s3cr3t")
        // console.log(user.role)
        if (user.role === "superadmin" || user.role === "admin") {
            next()
        } else {
            res.status(403).json({
                message: "Forbidden"
            })
        }
    } catch (err) {
        res.send(err)
    }

}

module.exports = carAuthenticator