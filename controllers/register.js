const { Users } = require("../models");
const encryptFunction = require("./encrypt_pass");

async function regist(req, res) {
  try {
    let usernameInput = req.body.username;
    let passwordInput = await encryptFunction(req.body.password);
    let addressInput = req.body.address;
    let phone_numberInput = req.body.phone_number;
    let fullnameInput = req.body.fullname;
    let newuser = await Users.create({
      username: usernameInput,
      password: passwordInput,
      role: "member",
      address: addressInput,
      phone_number: phone_numberInput,
      fullname: fullnameInput,
    });
    res.status(201).json({
      id: newuser.id,
      username: newuser.username,
      created_at: newuser.createdAt,
      updated_at: newuser.updatedAt,
    });
  } catch (err) {
    res.send(err);
  }
}

module.exports = regist
