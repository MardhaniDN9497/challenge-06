const { Cars } = require("../models");
const jwt = require("jsonwebtoken");

async function carDelete(req, res) {
  try {
    let check = await Cars.findOne({
      where: { id: req.params.id, is_deleted: false },
    });

    if (check) {
      let header = req.headers.authorization.split("Bearer ")[1];
      let user = jwt.verify(header, "s3cr3t");
      await Cars.update(
        { is_deleted: true, deleted_by: user.id },
        { where: { id: req.params.id } }
      );
      res.send("Data mobil berhasil dihapus");
      return
    } else {
      res.send("Data mobil tidak ada")
    }
  } catch (err) {
    res.send(err)
  }
}

module.exports = carDelete
