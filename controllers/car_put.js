const { Cars } = require("../models");
const jwt = require("jsonwebtoken");

async function carPut(req, res) {
  try {
    let header = req.headers.authorization.split("Bearer ")[1];
    let user = jwt.verify(header, "s3cr3t");
    let carData = await Cars.findOne({
      where: { id: req.params.id, is_deleted: false },
    });

    if (carData) {
      await Cars.update(
        {
          name: req.body.name,
          price: req.body.price,
          size_id: req.body.size_id,
          photo: req.body.photo,
          updated_by: user.id,
        },
        { where: { id: req.params.id } }
      );
      res.send("Data mobil berhasil diperbarui");
    } else {
      res.send("Data tidak ada");
    }
  } catch (err) {
    res.send(err)
  }
}

module.exports = carPut

