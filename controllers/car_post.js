const { Cars } = require("../models")
const jwt = require("jsonwebtoken");

async function carPost(req, res) {
    try {
        let header = req.headers.authorization.split("Bearer ")[1]
        let user = jwt.verify(header, "s3cr3t")
        await Cars.create({
            name: req.body.name,
            price: req.body.price,
            size_id: req.body.size_id,
            photo: req.body.photo,
            created_by: user.id,
            updated_by: user.id
        });
        res.status(201).json({
            name: req.body.name,
            price: req.body.price,
            size_id: req.body.size_id,
            photo: req.body.photo,
        })
    } catch (err) {
        res.send(err)
    }

}

module.exports = carPost