const { Users } = require("../models");

async function adminGetById(req, res) {
    try {
        let admin = await Users.findOne({
            where: { id: req.params.id, is_deleted: false },
        });

        if (admin == null || admin == undefined || admin == "") {
            res.send("Data Admin tidak ada");
            return;
        } else {
            res.status(201).json({
                id: admin.id,
                username: admin.username,
                password: admin.password,
                address: admin.address,
                phone_number: admin.phone_number,
                fullname: admin.fullname
            });
            return;
        }
    } catch (err) {
        res.send(err)
    }
}

module.exports = adminGetById
