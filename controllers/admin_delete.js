const { Users } = require("../models");

async function adminDelete(req, res) {
    try {
        let check = await Users.findOne({
            where: { id: req.params.id, role: "admin" },
        });
        if (check) {
            await Users.update(
                { is_deleted: true },
                { where: { id: req.params.id } }
            );
            res.send("Data admin berhasil dihapus");
            return
        }
    } catch (err) {
        res.send(err)
    }
}

module.exports = adminDelete
