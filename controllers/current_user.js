const jwt = require("jsonwebtoken");

async function userCurrent(req, res) {
  try {
    let header = req.headers.authorization.split("Bearer ")[1];
    let user = jwt.verify(header, "s3cr3t");
    if (user) {
      res.send(user.username);
    } else {
      res.send("Anda harus login dulu");
    }
  } catch (err) {
    res.send(err)
  }
}

module.exports = userCurrent
