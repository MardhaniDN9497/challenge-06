const jwt = require("jsonwebtoken")
const { send } = require("process")

function adminAuthenticator(req, res, next) {
    try {
        let header = req.headers.authorization.split("Bearer ")[1]
        let user = jwt.verify(header, "s3cr3t")
        if (user.role === "superadmin") {
            next()
        } else {
            res.status(403).json({
                message: "Forbidden"
            })
        }
    } catch (err) {
        send(err)
    }
}

module.exports = adminAuthenticator