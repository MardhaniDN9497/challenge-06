# Car Management API  
"Sebuah REST API yang dapat digunakan untuk melakukan manajemen data mobil dengan fitur authentication"

## How to run
guide 

To run the server, run:

```
npm start or yarn start
```

To view the Swagger UI interface:

```
open http://localhost:8080/docs
```

## Endpoints
POST("/api/v1/login") : untuk login bagi superadmin dan admin<br>
POST("/api/v1/register") : untuk mendaftar bagi member <br>
POST("/api/v1/admins") : untuk menambahkan data admin <br>
GET("/api/v1/admins") : untuk mendapatkan semua data admin <br>
GET("/api/v1/admins/:id") : untuk mendapatkan data admin berdasarkan id <br>
PUT("/api/v1/admins/:id") : untuk memperbarui data admin berdasarkan id <br>
DELETE("/api/v1/admins/:id") : untuk menghapus data admin berdasarkan id <br>
POST("/api/v1/cars") : untuk menambahkan data mobil <br>
GET("/api/v1/cars") : untuk mendapatkan semua data mobil <br>
GET("/api/v1/cars/:id") : untuk mendapatkan data mobil berdasarkan id <br>
PUT("/api/v1/cars/:id") : untuk memperbarui data mobil berdasarkan id <br>
DELETE("/api/v1/cars/:id") : untuk menghapus data mobil berdasarkan id <br>
GET("/api/v1/cars/:id") : untuk melihat user 

## ERD
![Entity Relationship Diagram](erd.png)