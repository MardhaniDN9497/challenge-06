'use strict';
const controllers = require("../controllers")
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  await queryInterface.bulkInsert("users", [
    {
      username: "superadmin@mail.com",
      password: await controllers.encryptPass("12345"),
      role: "superadmin",
      address: "Jakarta",
      phone_number: "082233128200",
      fullname: "Sabrina",
      is_deleted: false,
      created_at: new Date(),
      updated_at: new Date(),
    }
  ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
